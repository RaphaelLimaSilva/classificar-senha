import string

def classifica_senhas(pwd):

    nivel_senha = 0
    low = True
    high = True
    dig = True
    pun = True

    if len(pwd) <= 4 :
        return nivel_senha
    elif len(pwd) <=8 :
        nivel_senha += 0
    else :
        nivel_senha += 0.5

    for x in pwd:
        if  low and x in string.ascii_lowercase:
            low = False
            nivel_senha +=0.2
        elif high and  x in string.ascii_uppercase:
            high = False
            nivel_senha += 0.3
        elif dig and x in string.digits:
            dig = False
            nivel_senha += 0.5
        elif pun and x in string.punctuation :
            pun = False
            nivel_senha += 0.5
    return round(nivel_senha)




