import unittest
from Classifica import classifica_senhas

class TestStringMethods(unittest.TestCase):
    def test_classifica(self):
        self.assertEqual(
            classifica_senhas("aqWS12#$e"),2
        )
        self.assertEqual(
            classifica_senhas(""),0
        )
        self.assertEqual(
            classifica_senhas("aqw"),0
        )
        self.assertEqual(
            classifica_senhas("123qweASD"),2
        )
        self.assertEqual(
            classifica_senhas("qwAS12!"),2
        )

        self.assertEqual(
            classifica_senhas("aQ1@"),0
        )




def runTests():
    suite = unittest.defaultTestLoader.loadTestsFromTestCase(TestStringMethods)
    unittest.TextTestRunner(verbosity=2, failfast=True).run(suite)

if __name__ == '__main__':
    unittest.main()